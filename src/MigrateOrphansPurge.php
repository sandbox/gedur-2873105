<?php

namespace Drupal\migrate_orphans;

use Drupal\migrate\Exception\RequirementsException;
use Drupal\migrate_tools\MigrateExecutable;

use Drupal\migrate\Event\MigrateRowDeleteEvent;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;

/**
 * Class MigrateOrphansPurge migration purge manager.
 */
class MigrateOrphansPurge extends MigrateExecutable {

  /**
   * Dry Run.
   *
   * @var bool
   */
  protected $dryRun;

  /**
   * {@inheritdoc}
   */
  public function __construct(MigrationInterface $migration, MigrateMessageInterface $message, array $options = []) {
    parent::__construct($migration, $message, $options);
    if (isset($options['dry-run'])) {
      $this->dryRun = $options['dry-run'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function import() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function rollback() {
    return NULL;
  }

  /**
   * Removes source deleted items imported previously.
   *
   * Loads current migrate map and compares it with the imported list,
   * after that removes the not found entities.
   */
  public function purge() {

    // Only begin the purge operation if the migration is currently idle.
    if ($this->migration->getStatus() !== MigrationInterface::STATUS_IDLE) {
      $this->message->display($this->t('Migration @id is busy with another operation: @status',
        [
          '@id' => $this->migration->id(),
          '@status' => $this->migration->getStatusLabel(),
        ]), 'error');
      return MigrationInterface::RESULT_FAILED;
    }
    $this->getEventDispatcher()->dispatch(MigrateEvents::PRE_IMPORT, new MigrateImportEvent($this->migration, $this->message));

    // Knock off migration if the requirements haven't been met.
    try {
      $this->migration->checkRequirements();
    }
    catch (RequirementsException $e) {
      $this->message->display(
        $this->t(
          'Migration @id did not meet the requirements. @message @requirements',
          [
            '@id' => $this->migration->id(),
            '@message' => $e->getMessage(),
            '@requirements' => $e->getRequirementsString(),
          ]
        ),
        'error'
      );

      return MigrationInterface::RESULT_FAILED;
    }

    // @TODO: remove?
    $this->migration->setStatus(MigrationInterface::STATUS_IMPORTING);

    $return = MigrationInterface::RESULT_COMPLETED;

    $source = $this->getSource();
    $id_map = $this->migration->getIdMap();

    try {
      $source->rewind();
    }
    catch (\Exception $e) {
      $this->message->display(
        $this->t('Migration failed with source plugin exception: @e', ['@e' => $e->getMessage()]), 'error');
      $this->migration->setStatus(MigrationInterface::STATUS_IDLE);
      return MigrationInterface::RESULT_FAILED;
    }

    $destination = $this->migration->getDestinationPlugin();

    // Find current source items:
    $source_ids = [];
    while ($source->valid()) {
      $row = $source->current();
      $source_ids[] = $row->getSourceIdValues();

      try {
        $source->next();
      }
      catch (\Exception $e) {
        $this->message->display(
          $this->t('Migration failed with source plugin exception: @e',
            ['@e' => $e->getMessage()]), 'error');
        $this->migration->setStatus(MigrationInterface::STATUS_IDLE);
        return MigrationInterface::RESULT_FAILED;
      }
    }

    // Loop through each row in the map, and purge it orphaned items.
    $purged_total = 0;
    foreach ($id_map as $map_row) {
      $destination_key = $id_map->currentDestination();
      $source_key = $id_map->currentSource();

      // Find if the current source exists.
      $delete = TRUE;
      foreach ($source_ids as $source_id) {
        if ($source_id === $source_key) {
          $delete = FALSE;
          break;
        }
      }

      // Element does not exist anymore.
      if ($delete) {
        $purged_total++;

        // Dry run.
        if ($this->dryRun) {

          $this->message->display($this->t('Item source @source with destination @destination will be purged',
            [
              '@source' => str_replace('"', "", json_encode($source_key)),
              '@destination' => str_replace('"', "", json_encode($destination_key)),
            ]), 'status');

          continue;
        }

        // Find imported items.
        if ($destination_key) {
          $map_row = $id_map->getRowByDestination($destination_key);
          if ($map_row['rollback_action'] == MigrateIdMapInterface::ROLLBACK_DELETE) {
            $this->getEventDispatcher()
              ->dispatch(MigrateEvents::PRE_ROW_DELETE, new MigrateRowDeleteEvent($this->migration, $destination_key));
            $destination->rollback($destination_key);
            $this->getEventDispatcher()
              ->dispatch(MigrateEvents::POST_ROW_DELETE, new MigrateRowDeleteEvent($this->migration, $destination_key));
          }
          // We're now done with this row, so remove it from the map.
          $id_map->deleteDestination($destination_key);
        }
        else {
          // If there is no destination key the import probably failed and we
          // can remove the row without further action.
          $source_key = $id_map->currentSource();
          $id_map->delete($source_key);
        }

        // Check for memory exhaustion.
        if (($return = $this->checkStatus()) != MigrationInterface::RESULT_COMPLETED) {
          break;
        }

        // If anyone has requested we stop, return the requested result.
        if ($this->migration->getStatus() == MigrationInterface::STATUS_STOPPING) {
          $return = $this->migration->getInterruptionResult();
          $this->migration->clearInterruptionResult();
          break;
        }
      }

    }

    // Find imported items.
    $this->message->display($this->t('Purged @total items',
      [
        '@total' => $purged_total,
      ]), 'status');

    $this->migration->setStatus(MigrationInterface::STATUS_IDLE);
    return $return;
  }

}
