<?php

namespace Drupal\migrate_orphans\Commands;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_orphans\MigrateOrphansPurge;
use Drupal\migrate_tools\Commands\MigrateToolsCommands;
use Drupal\migrate_tools\Drush9LogMigrateMessage;

/**
 * Commands used for omnicanal poi migrations.
 */
class MigrateOrphansCommands extends MigrateToolsCommands {

  /**
   * Setup destination directory of correos pickup points.
   *
   * @command migrate-orphans
   *
   * @option dry-run Do not delete, just report the records.
   *
   * @field-labels
   *   dry-run: Dry run
   * @default-fields dry-run
   */
  public function migrateOrphans($migration_ids = '', array $options = ['dry-run' => FALSE]) {
    if (!$migration_ids) {
      drush_set_error('MIGRATE_ERROR', dt('You must specify one or more migration names separated by commas'));
      return;
    }

    $migrate_orphans_execute_options = [];
    foreach (['dry-run'] as $option) {
      if (isset($options[$option])) {
        $migrate_orphans_execute_options[$option] = $options[$option];
      }
    }

    $migrations = $this->migrationsList($migration_ids);
    if (empty($migrations)) {
      $this->logger()->error(dt('No migrations found.'));
    }

    // Take it one group at a time, importing the migrations within each group.
    foreach ($migrations as $migration_list) {
      array_walk($migration_list, function (MigrationInterface $migration) use ($migrate_orphans_execute_options) {
        $this->purgeMigration($migration, $migrate_orphans_execute_options);
      });
    }

  }

  /**
   * Purge a migration.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   Migration name.
   * @param array $options
   *   Options.
   *
   * @throws \Drupal\migrate\MigrateException
   */
  protected function purgeMigration(MigrationInterface $migration, array $options) {
    $log = new Drush9LogMigrateMessage($this->logger());

    // Take into account all migrated items:
    $migration->getIdMap()->prepareUpdate();

    $executable = new MigrateOrphansPurge($migration, $log, $options);
    // drush_op() provides --simulate support ??
    drush_op([$executable, 'purge']);
  }

}
